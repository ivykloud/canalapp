test.skip('not a test file', () => 1)

export const mockMoviesList = [
  {
    id: 508947,
    original_title: 'Turning Red',
    title: 'Turning Red',
  },
  {
    id: 785985,
    original_title: 'Loin du périph',
    title: 'The Takedown',
  },
  {
    id: 406759,
    original_title: 'Moonfall',
    title: 'Moonfall',
  },
  {
    id: 763285,
    original_title: 'Ambulance',
    title: 'Ambulance',
  },
  {
    id: 893370,
    original_title: 'Virus-32',
    title: 'Virus:32',
  },
  {
    id: 883502,
    original_title: "Fortress: Sniper's Eye",
    title: "Fortress: Sniper's Eye",
  },
  {
    id: 284052,
    original_title: 'Doctor Strange',
    title: 'Doctor Strange',
  },
  {
    id: 799876,
    original_title: 'The Outfit',
    title: 'The Outfit',
  },
  {
    id: 568124,
    original_title: 'Encanto',
    title: 'Encanto',
  },
]

export const mockMovie = {
  genres: [
    {
      id: 16,
      name: 'Animation',
    },
    {
      id: 35,
      name: 'Comedy',
    },
    {
      id: 10751,
      name: 'Family',
    },
    {
      id: 14,
      name: 'Fantasy',
    },
  ],
  id: 568124,
  original_title: 'Encanto',
  overview:
    "The tale of an extraordinary family, the Madrigals, who live hidden in the mountains of Colombia, in a magical house, in a vibrant town, in a wondrous, charmed place called an Encanto. The magic of the Encanto has blessed every child in the family with a unique gift from super strength to the power to heal—every child except one, Mirabel. But when she discovers that the magic surrounding the Encanto is in danger, Mirabel decides that she, the only ordinary Madrigal, might just be her exceptional family's last hope.",
  tagline: "There's a little magic in all of us ...almost all of us.",
  title: 'Encanto',
  vote_average: 7.7,
  vote_count: 6487,
}
