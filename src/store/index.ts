import { CombinedState, configureStore } from '@reduxjs/toolkit'
import { combineEpics, createEpicMiddleware } from 'redux-observable'

import { convertModuleToArray } from '../helpers/convertModuleToArray'

import { MoviesState, reducer as movies } from './movies/reducer'
import { AppState, reducer as app } from './app/reducer'

import * as moviesEpics from './movies/epics'

export type RootState = CombinedState<{
  app: AppState
  movies: MoviesState
}>

const epicMiddleware = createEpicMiddleware()

export const rootEpic = combineEpics(...convertModuleToArray(moviesEpics))

export const store = configureStore({
  reducer: {
    app,
    movies,
  },
  middleware: [epicMiddleware],
})

epicMiddleware.run(rootEpic)
