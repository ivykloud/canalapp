# Mini Movies Listing App
 
This is a small React app that can 
- list some movies from an api
- show details of a selected movie
## Used Librairies

### `Redux`

Used as state manager to centralize the whole state tree in a single store

### `RxJS / redux-observable`

Used as middleware for redux, it allows asynchronous actions to trigger multiple effects on a single action on the store

### `styled-components`

For styling