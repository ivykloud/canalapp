import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import { getSelectedMovie } from '../store/movies/selectors'

const DetailsCard = styled.div`
  min-height: 80px;
  text-align: center;
  border: 2px solid #111;
  border-radius: 10px;
  margin-top: 20px;

  h3 {
    margin: 0;
  }
`

export const MovieDetailsCard = () => {
  const movie = useSelector(getSelectedMovie)
  if (movie) {
    const { title, original_title, overview, tagline, genres, vote_average, vote_count } = movie
    const shouldShowOriginalTitle = title !== original_title
    return (
      <DetailsCard>
        <h3>
          {title} {shouldShowOriginalTitle && <i>({original_title})</i>}
        </h3>
        {tagline}
        <p>Genres : {genres.map((genre) => genre.name).join(', ')}</p>
        <p>{overview}</p>
        <p>Ratings : {vote_average}/10 ({vote_count} votes)</p>
        
      </DetailsCard>
    )
  } else {
    return null
  }
}
