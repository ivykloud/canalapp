import { SET_MOVIES, SET_SELECTED_ID, SET_SELECTED_MOVIE } from '../constants'
import { defaultState, reducer } from '../reducer'
import { mockMovie, mockMoviesList } from './mocks'

describe('movies > reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, { type: null, payload: null })).toMatchObject(defaultState)
  })
  it(`should handle ${SET_MOVIES}`, () => {
    expect(reducer(defaultState, { type: SET_MOVIES, payload: { movies: mockMoviesList } })).toEqual({
      ...defaultState,
      list: mockMoviesList,
    })
  })
  it(`should handle ${SET_SELECTED_ID}`, () => {
    expect(reducer(defaultState, { type: SET_SELECTED_ID, payload: { id: mockMovie.id } })).toEqual({
      ...defaultState,
      selectedId: mockMovie.id,
    })
  })
  it(`should handle ${SET_SELECTED_MOVIE}`, () => {
    expect(reducer(defaultState, { type: SET_SELECTED_MOVIE, payload: { movie: mockMovie } })).toEqual({
      ...defaultState,
      selected: mockMovie,
    })
  })
})
