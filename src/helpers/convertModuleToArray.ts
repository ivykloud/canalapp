export const convertModuleToArray = (mod) => {
  const moduleContentList = []
  for (const key in mod) {
    moduleContentList.push(mod[key])
  }
  return moduleContentList
}
