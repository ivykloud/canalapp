import { Action } from 'redux'
import { INIT_APP } from './constants'

export type InitAppAction = Action<typeof INIT_APP>

export const initApp: () => InitAppAction = () => ({
  type: INIT_APP,
})
