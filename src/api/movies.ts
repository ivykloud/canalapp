import { ajax } from 'rxjs/ajax'
import { map } from 'rxjs/operators'

import { config } from './config'

export const getMoviesList = () =>
  ajax({
    url: `${config.url}/discover/movie?api_key=${config.api_key}`,
    method: 'GET',
  }).pipe(map((data) => data.response))

export const getMovieDetails = (id: number) =>
  ajax({
    url: `${config.url}/movie/${id}?api_key=${config.api_key}`,
    method: 'GET',
  }).pipe(map((data) => data.response))
