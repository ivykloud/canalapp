import { RootState } from '..'
import { MoviesState } from './reducer'

const getMoviesState = (state: RootState): MoviesState => state.movies

export const getMoviesList = (state: RootState): MoviesState['list'] => getMoviesState(state).list
export const getSelectedMovieId = (state: RootState): MoviesState['selectedId'] => getMoviesState(state).selectedId

export const getSelectedMovie = (state: RootState): MoviesState['selected'] => getMoviesState(state).selected
export const getAlreadySelectedMovieId = (state: RootState): number => getSelectedMovie(state)?.id
