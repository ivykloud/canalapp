import { ofType, StateObservable } from 'redux-observable'
import { EMPTY, map, Observable, switchMap, withLatestFrom } from 'rxjs'
import { RootState } from '..'
import { INIT_APP } from '../app/constants'
import { getMovieDetails, getMoviesList } from '../../api/movies'

import { InitAppAction } from '../app/actions'
import { setMoviesList, SetSelectedIdAction, setSelectedMovie } from './actions'
import { SET_SELECTED_ID } from './constants'
import { getAlreadySelectedMovieId } from './selectors'

export const initMoviesList = (action$: Observable<InitAppAction>, state$: StateObservable<RootState>) =>
  action$.pipe(
    ofType(INIT_APP),
    withLatestFrom(state$),
    switchMap(() => getMoviesList().pipe(map((movies: any) => setMoviesList(movies?.results)))),
  )

export const setMovieDetails = (action$: Observable<SetSelectedIdAction>, state$: StateObservable<RootState>) =>
  action$.pipe(
    ofType(SET_SELECTED_ID),
    withLatestFrom(state$),
    switchMap(([action, state]) => {
      const id = action.payload.id
      const alreadySelectedId = getAlreadySelectedMovieId(state)

      return id !== alreadySelectedId ? getMovieDetails(id).pipe(map((movie: any) => setSelectedMovie(movie))) : EMPTY
    }),
  )
