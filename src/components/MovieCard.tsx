import React from 'react'
import { useDispatch } from 'react-redux'
import { setSelectedId } from 'store/movies/actions'
import styled from 'styled-components'
import { Movie } from 'types'

const Card = styled.div`
  display: flex;
  height: 80px;
  min-width: 200px;
  justify-content: center;
  align-items: center;
  text-align: center;
  border: 2px solid #111;
  border-radius: 10px;
  cursor: pointer;
  flex-direction: column;
  flex: 1 1 0;
  
  :hover {
    border-color: #3db5b5;
    color: #3db5b5;
  }

  h3 {
    margin: 0;
  }
`

export const MovieCard = ({ title, original_title, id }: Movie) => {
  const dispatch = useDispatch()
  const shouldShowOriginalTitle = title !== original_title
  return (
    <Card onClick={() => dispatch(setSelectedId(id))}>
      <h3>{title}</h3>
      {shouldShowOriginalTitle && <i>({original_title})</i>}
    </Card>
  )
}
