export type Movie = {
  title: string
  original_title: string
  id: number
}

export type MovieDetails = Movie & {
  overview: string
  tagline: string
  genres: Genre[]
  vote_average: number
  vote_count: number
}

export type Genre = {
  id:number
  name: string
}