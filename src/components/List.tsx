import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import { getMoviesList } from '../store/movies/selectors'

import { MovieCard } from './MovieCard'
const ListContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 5px;
`

export const List = () => {
  const movies = useSelector(getMoviesList)

  return (
    <ListContainer>
      {movies.map((movieInfo) => (
        <MovieCard {...movieInfo} key={movieInfo.id} />
      ))}
    </ListContainer>
  )
}
