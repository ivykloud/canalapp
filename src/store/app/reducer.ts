import { INIT_APP } from "./constants"

export type AppState = {
  init: boolean
}

const defaultState: AppState = {
  init: false,
}

const defaultAction = { type: '' }

export const reducer = (state = defaultState, { type } = defaultAction) => {
  switch (type) {
    case INIT_APP:
      return { ...state, init: true }
    default:
      return state
  }
}
