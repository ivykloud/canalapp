import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import { initApp } from 'store/app/actions'

import { List } from './components/List'
import { MovieDetailsCard } from './components/MovieDetailsCard'

const Page = styled.div`
  font-family: Cambria;
  font-size: 12px;
`

const mapDispatchToProps = (dispatch) => ({
  onInitApp: () => {
    dispatch(initApp())
  },
})

export const App = ({ onInitApp = () => null }) => {
  const [isInit, setInit] = useState(false)

  useEffect(() => {
    if (!isInit) {
      onInitApp()
      setInit(true)
    }
  }, [isInit, onInitApp])

  return (
    <Page>
      <List />
      <MovieDetailsCard />
    </Page>
  )
}

export default connect(null, mapDispatchToProps)(App)
