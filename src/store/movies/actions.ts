import { Action } from 'redux'
import { Movie } from '../../types'
import { SET_MOVIES, SET_SELECTED_ID, SET_SELECTED_MOVIE } from './constants'

export type SetMoviesAction = Action<typeof SET_MOVIES> & {
  payload: {
    movies: Movie[]
  }
}
export type SetSelectedIdAction = Action<typeof SET_SELECTED_ID> & {
  payload: {
    id: number
  }
}

export type SetSelectedMovieAction = Action<typeof SET_SELECTED_MOVIE> & {
  payload: {
    movie: Movie
  }
}

export const setMoviesList = (movies: Movie[]): SetMoviesAction => ({
  payload: { movies },
  type: SET_MOVIES,
})

export const setSelectedId = (id: number): SetSelectedIdAction => ({
  payload: { id },
  type: SET_SELECTED_ID,
})

export const setSelectedMovie = (movie: Movie): SetSelectedMovieAction => ({
  payload: { movie },
  type: SET_SELECTED_MOVIE,
})
