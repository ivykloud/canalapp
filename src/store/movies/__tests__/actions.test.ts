import { setMoviesList, setSelectedId, setSelectedMovie } from '../actions'
import { SET_MOVIES, SET_SELECTED_ID, SET_SELECTED_MOVIE } from '../constants'
import { mockMovie, mockMoviesList } from './mocks'

describe('movies > actions', () => {
  it('should create an action that changes movies list', () => {
    const expectedAction = {
      type: SET_MOVIES,
      payload: {
        movies: mockMoviesList,
      },
    }
    expect(setMoviesList(mockMoviesList)).toEqual(expectedAction)
  })

  it('should create an action that sets selected id', () => {
    const expectedAction = {
      type: SET_SELECTED_ID,
      payload: {
        id: mockMovie.id,
      },
    }
    expect(setSelectedId(mockMovie.id)).toEqual(expectedAction)
  })

  it('should create an action that sets selected movie', () => {
    const expectedAction = {
      type: SET_SELECTED_MOVIE,
      payload: {
        movie: mockMovie,
      },
    }
    expect(setSelectedMovie(mockMovie)).toEqual(expectedAction)
  })
})
