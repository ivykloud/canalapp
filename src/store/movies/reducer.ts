import { Movie, MovieDetails } from 'types'
import { SetMoviesAction, SetSelectedIdAction, SetSelectedMovieAction } from './actions'
import { SET_MOVIES, SET_SELECTED_ID, SET_SELECTED_MOVIE } from './constants'

export type MoviesState = {
  selectedId: number
  list: Movie[]
  selected: MovieDetails
}

export const defaultState: MoviesState = {
  selectedId: null,
  list: [],
  selected: null,
}

const defaultAction = { type: '', payload: null }

export const reducer = (state = defaultState, { type, payload } = defaultAction) => {
  switch (type) {
    case SET_MOVIES:
      return {
        ...state,
        list: (payload as SetMoviesAction['payload']).movies,
      }
    case SET_SELECTED_ID:
      return {
        ...state,
        selectedId: (payload as SetSelectedIdAction['payload']).id,
      }
    case SET_SELECTED_MOVIE:
      return {
        ...state,
        selected: (payload as SetSelectedMovieAction['payload']).movie,
      }
    default:
      return state
  }
}
