import { RootState } from '../..'
import { getAlreadySelectedMovieId, getMoviesList, getSelectedMovie, getSelectedMovieId } from '../selectors'
import { mockMovie, mockMoviesList } from './mocks'

export const rootState: RootState = {
  app: {
    init: true,
  },
  movies: {
    list: mockMoviesList,
    selected: mockMovie,
    selectedId: mockMovie.id,
  },
}

describe('movies > selectors', () => {
  it('should return movies list', () => {
    expect(getMoviesList(rootState)).toEqual(rootState.movies.list)
  })

  it('should return new selected id', () => {
    expect(getSelectedMovieId(rootState)).toEqual(rootState.movies.selectedId)
  })

  it('should return selected movie', () => {
    expect(getSelectedMovie(rootState)).toEqual(rootState.movies.selected)
  })
  
  it('should return selected movie id', () => {
    expect(getAlreadySelectedMovieId(rootState)).toEqual(rootState.movies.selected.id)
  })
})
